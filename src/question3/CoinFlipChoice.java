package question3;

import javafx.event.EventHandler;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;
//Created this class to add the eventHandlers
public class CoinFlipChoice implements EventHandler<ActionEvent>{
	private String input;
	private Text msg;
	private Text money;
	private TextField bet;
	private CoinFlipGame cfg = new CoinFlipGame();
	
	public CoinFlipChoice(String input, Text msg, Text money, TextField bet, CoinFlipGame cfg) {
		this.input = input;
		this.msg = msg;
		this.money = money;
		this.bet = bet;
		this.cfg = cfg;
	}
	
    @Override
    public void handle(ActionEvent e) {
        String msgTemp = cfg.playRound(input);
        msg.setText(msgTemp);
        int moneyTemp = cfg.getMoney();
        money.setText(String.valueOf(moneyTemp));
        bet.getText();
    } 
}
