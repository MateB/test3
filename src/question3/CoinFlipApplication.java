package question3;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class CoinFlipApplication extends Application {
	
	@Override
	public void start(Stage stage)
	{
		//creating hbox' and vbox for storing the fields
		Group root = new Group();
		VBox vb = new VBox();
		HBox hb1 = new HBox();
		HBox hb2 = new HBox();
		HBox hb3 = new HBox();
		HBox hb4 = new HBox();
		HBox hb5 = new HBox();
		CoinFlipGame cfg = new CoinFlipGame();
		CoinFlipChoice obj;
		
		//creating the different fields
		Text msg = new Text();
		Text balance = new Text("Hi there heres your balance: " + cfg.getMoney());
		Text betting = new Text("How much do you want to bet?");
		TextField bet = new TextField();
		Button head = new Button("heads");
		Button tail = new Button("tails");
		
		//adding the actions to the buttons
		obj = new CoinFlipChoice("head", msg, balance, bet, cfg);
		head.setOnAction(obj);
		obj = new CoinFlipChoice("tail", msg, balance, bet, cfg );
		tail.setOnAction(obj);
		
		
		//adding the different textfields to the boxes
		hb1.getChildren().addAll(head, tail);
		hb2.getChildren().add(msg);
		hb3.getChildren().add(balance);
		hb4.getChildren().add(betting);
		hb5.getChildren().add(bet);
		vb.getChildren().addAll(hb1, hb2, hb3, hb4, hb5);
		root.getChildren().add(vb);
		
		//creating the scene
		Scene scene = new Scene(root, 600, 600);
		scene.setFill(Color.BLACK);
		
		//showing the stage		
		stage.setTitle("Coin Flip");
		stage.setScene(scene);
		stage.show();		
	}
	
	public static void main(String[] args)
	{
		Application.launch(args);
	}	
}
