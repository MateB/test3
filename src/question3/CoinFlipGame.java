package question3;

import java.util.*;

public class CoinFlipGame {
	
	Scanner reader = new Scanner(System.in);
	int money = 100;
	int bet = reader.nextInt();
	Random rand = new Random();
	//get method for the money
	public int getMoney()
	{
		return this.money;
	}
	//get method for the bet
	public int getBet()
	{
		return this.bet;
	}
	//playing a round of cloin flip
	public String playRound(String input)
	{
		int num = rand.nextInt(2);
		//check if the bet is valid
		if(this.bet > this.money || this.bet <= 0)
		{
			return "Invalid bet";
		}
		//if its valid it proceeds to check the input from the user vs the generated heads or tails
		else
		{
			//if its 1 then its head
			if (num == 1)
			{
				//if input is equal to heads they won their bet money else they lost it.
				if (input.equals("heads"))
				{
					this.money += this.bet;
					return "you chose: " + input + "it was heads" + "your money: " + this.money;
				}
				else
				{
					this.money-=this.bet;
					return "you chose: " + input + "it was tails" + "your money: " + this.money;
				}
				
			}
			//if its not 1 it means its tails
			else
			{
				//if input is equal to tails they won else they lose their bet money
				if(input.equals("tails"))
				{
					this.money += this.bet;
					return "you chose: " + input + "it was tails" + "your money: " + this.money;
				}
				else
				{
					this.money-=this.bet;
					return "you chose: " + input + "it was tails" + "your money: " + this.money;
				}
			}
		}

	}
}
