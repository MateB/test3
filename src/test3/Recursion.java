package test3;

public class Recursion 
{
	public static void main(String[] args)
	{
		int[] numbers = {2,3,4,5,6,7,8,20};
		int n = 0;
		System.out.println(recursiveCount(numbers,n));
	}
	
	public static int recursiveCount(int[] numbers, int n)
	{
		int count = 0;
		if(n < numbers.length)
		{
			if (numbers[n] < 10 && numbers[n] % 2 == 0)
			{
				count = recursiveCount(numbers, n+1)+1;
				return count;
			}
			count = recursiveCount(numbers, n+1);
			return count;
		}
		return count;
	}
}
